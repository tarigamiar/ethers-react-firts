import "./style/fields.css";

const capitalizeFirstLetter = (str) => {
    return str.charAt(0).toUpperCase() + str.slice(1);
}

const formatField = (field) => {
    return capitalizeFirstLetter(field.replace(/[A-Z]/g, ' $&').trim().toLowerCase());
}

function Fields(props) {
    return (
        props.ready ? (
            <div className="fields">
                {Object.keys(props).map((key) => {
                    if (key !== "ready" && key !== "notReadyText" && props[key].trim() !== ""){
                        return (<span key={key} className="field"><b>{formatField(key)}:</b> {props[key]}</span>)
                    }
                    return null
                })}
            </div>) : (
            <div className="fields">
                <span className="field" key="notReadyText">{props.notReadyText}</span>
            </div>
        )
    )
}

export default Fields;