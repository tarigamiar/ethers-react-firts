import { provider } from '../setupEthers.js'
import './style/contract.css'
import Fields from './fields.jsx';
import React, { useState } from 'react'

const ethers = require('ethers')

function Balance() {
  const [address, setAddress] = useState('')
  const [notReadyText, setNotReadyText] = useState('')
  const [ready, setReady] = useState(false)
  const [balance, setBalance] = useState('')

  const handleSubmit = async (event) => {
    event.preventDefault()
    if (address < 42) {
      setNotReadyText('Please enter a valid address')
      return
    }
    setReady(false)
    setNotReadyText('wait for result..')
    const bal = await provider.getBalance(address)
    setBalance(ethers.utils.formatEther(bal))
    setReady(true)
  }

  const handleClear = async (event) => {
    setAddress('')
    setNotReadyText('')
    setBalance('')
    setReady(false)
  }

  return (
    <div className="contract-container">
      <h2>Balance</h2>
      <input type="text" className="address-input" value={address} placeholder="Wallet address" onChange={(event) => { setAddress(event.target.value) }} />
      <button onClick={handleSubmit}>Show balance</button>
      <button onClick={handleClear}>Clear data</button>
      <Fields ready={ready}
        notReadyText={notReadyText}
        balance={balance} />
    </div>
  )
}

export default Balance