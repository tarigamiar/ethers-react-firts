import { provider, ERC20_ABI } from '../setupEthers.js'
import React, { useState } from 'react'
import './style/contract.css'
import Fields from './fields';

const ethers = require('ethers')

function Contract() {
    const [address, setAddress] = useState('')
    const [notReadyText, setNotReadyText] = useState('')
    const [ready, setReady] = useState(false)
    const [balance, setBalance] = useState('')
    const [name, setName] = useState('')
    const [symbol, setSymbol] = useState('')
    const [totalSupply, setTotalSupply] = useState('')

    const handleSubmit = async (event) => {
        event.preventDefault()
        if (address < 42) {
            setNotReadyText('Please enter a valid address')
            return
        }
        setNotReadyText('wait for result..')
        setReady(false)
        const contract = new ethers.Contract(address, ERC20_ABI, provider)
        setName(await contract.name())
        setSymbol(await contract.symbol())
        setTotalSupply(ethers.utils.formatEther(await contract.totalSupply()))
        setBalance(ethers.utils.formatEther(await contract.balanceOf(address)))
        setReady(true)
    }

    const handleClear = async (event) => {
        setAddress('')
        setNotReadyText('')
        setReady(false)
    }

    return (
        <div className="contract-container">
            <h2>Contract</h2>
            <input type="text" className="address-input" value={address} placeholder="Contract address" onChange={(event) => { setAddress(event.target.value) }} />
            <button className="submit-button" onClick={handleSubmit}>Find contract</button>
            <button onClick={handleClear}>Clear data</button>

            <Fields ready={ready}
                notReadyText={notReadyText}
                name={name}
                symbol={symbol}
                totalSupply={totalSupply}
                balance={balance} />
        </div>
    )
}

export default Contract