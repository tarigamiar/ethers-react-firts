import { provider, ERC20_ABI } from '../setupEthers.js'
import React, { useState } from 'react'
import './style/contract.css'
import Fields from './fields';

const ethers = require('ethers')

function Transfer() {
    const [ownedAccountAddress, setownedAccountAddress] = useState('')
    const [otherAccountAddress, setotherAccountAddress] = useState('')
    const [amount, setAmount] = useState('')
    const [privateKey, setPrivateKey] = useState('')
    const [senderBalanceBefore, setSenderBalanceBefore] = useState('')
    const [receiverBalanceBefore, setReceiverBalanceBefore] = useState('')
    const [senderBalanceAfter, setSenderBalanceAfter] = useState('')
    const [receiverBalanceAfter, setReceiverBalanceAfter] = useState('')
    const [notReadyText, setNotReadyText] = useState('')
    const [ready, setReady] = useState(false)

    const handleSubmit = async (event) => {
        event.preventDefault()
        if (ownedAccountAddress < 42 || otherAccountAddress < 42 || privateKey < 64) {
            setNotReadyText('Please enter a valid address')
            return
        }
        setNotReadyText('wait for result..')
        setReady(false)
        try {
            const wallet = new ethers.Wallet(privateKey, provider)
            setSenderBalanceBefore(ethers.utils.formatEther(await provider.getBalance(ownedAccountAddress)))
            setReceiverBalanceBefore(ethers.utils.formatEther(await provider.getBalance(otherAccountAddress)))
            await wallet.sendTransaction({
                to: otherAccountAddress,
                value: ethers.utils.parseEther(amount),
            }).then(async (tx) => {
                setNotReadyText('wait for blocks confirmation..')
                await tx.wait()
                setSenderBalanceAfter(ethers.utils.formatEther(await provider.getBalance(ownedAccountAddress)))
                setReceiverBalanceAfter(ethers.utils.formatEther(await provider.getBalance(otherAccountAddress)))
                console.log(tx)
                setReady(true)
            }).catch(async (error) => {
                setNotReadyText('error occured, see in browser console')
                setReady(false)
                console.log(error)
            })

        }
        catch (error) {
            setNotReadyText('error occured, see in browser console')
            setReady(false)
            console.log(error)
        }
    }

    const handleClear = async (event) => {
        event.preventDefault()
        setownedAccountAddress('')
        setotherAccountAddress('')
        setAmount('')
        setPrivateKey('')
        setSenderBalanceBefore('')
        setReceiverBalanceBefore('')
        setSenderBalanceAfter('')
        setReceiverBalanceAfter('')
        setNotReadyText('')
        setReady(false)
    }

    return (
        <div className="contract-container">
            <h2>Transfert</h2>
            <input type="text" className="address-input" value={ownedAccountAddress} placeholder="Your account public address" onChange={(event) => {setownedAccountAddress(event.target.value)}} />
            <input type="text" className="address-input" value={otherAccountAddress} placeholder="Other account address" onChange={(event) => {setotherAccountAddress(event.target.value)}} />
            <input type="password" className="address-input" value={privateKey} placeholder="Your private key" onChange={(event) => {setPrivateKey(event.target.value)}} />

            <input type="text" className="input" value={amount} placeholder="Amount" onChange={(event) => {setAmount(event.target.value)}} />
            <button className="submit-button" onClick={handleSubmit}>Submit</button>
            <button onClick={handleClear}>Clear data</button>

            <Fields ready={ready}
                notReadyText={notReadyText}
                senderBalanceBefore={senderBalanceBefore}
                receiverBalanceBefore={receiverBalanceBefore}
                senderBalanceAfter={senderBalanceAfter}
                receiverBalanceAfter={receiverBalanceAfter} />
        </div>
    )
}

export default Transfer
