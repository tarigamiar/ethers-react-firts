import { ThemeContext } from "../context/themeContext";
import React from "react";
import "./style/toolbar.css";

function ThemedButton(props) {
    const theme = React.useContext(ThemeContext);

    return(
        <button id="theme-button" style={{backgroundColor: theme.background}} onClick={props.onClick}>
            Change theme color
        </button>
    );
}

function Toolbar(props) {
    return(
        <div id="toolbar" >
            <ThemedButton onClick={props.toggleTheme}></ThemedButton>
        </div>
    );
}

export default Toolbar;