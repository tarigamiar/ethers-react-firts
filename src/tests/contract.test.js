import Contract from '../components/contract';
import { render, screen, waitFor} from '@testing-library/react';
import userEvent from "@testing-library/user-event";

jest.setTimeout(10000)

test('test find contract', async () => {
    render(<Contract />);
    const findContract = screen.getByText(/Find contract/i);
    userEvent.click(findContract);
    let linkElement = screen.getByText(/Please enter a valid address/i);
    expect(linkElement).toBeInTheDocument();
    userEvent.type(screen.getByPlaceholderText(/Contract address/i), '0xdAC17F958D2ee523a2206206994597C13D831ec7');
    userEvent.click(findContract);
    linkElement = screen.getByText(/wait for result../i);
    expect(linkElement).toBeInTheDocument();
    waitFor(() => {
        expect(screen.getByText(/Name:/i)).toBeInTheDocument();
    }, { timeout: 5000, interval: 50 });
    waitFor(() => {
        expect(screen.getByText(/Symbol:/i)).toBeInTheDocument();
    }, { timeout: 1000, interval: 50 });
    waitFor(() => {
        expect(screen.getByText(/Total supply:/i)).toBeInTheDocument();
    }, { timeout: 1000, interval: 50 });
    waitFor(() => {
        expect(screen.getByText(/Balance:/i)).toBeInTheDocument();
    }, { timeout: 1000, interval: 50 });
})