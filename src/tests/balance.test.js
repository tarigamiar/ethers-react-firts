import Balance from '../components/balance';
import { render, screen, waitFor} from '@testing-library/react';
import userEvent from "@testing-library/user-event"

jest.setTimeout(10000)

test('test show balance', async () => {
    render(<Balance />);
    const showBalance = screen.getByText(/Show balance/i);
    userEvent.click(showBalance);
    let linkElement = screen.getByText(/Please enter a valid address/i);
    expect(linkElement).toBeInTheDocument();
    userEvent.type(screen.getByPlaceholderText(/Wallet address/i), '0x6B175474E89094C44Da98b954EedeAC495271d0F');
    userEvent.click(showBalance);
    linkElement = screen.getByText(/wait for result../i);
    expect(linkElement).toBeInTheDocument();
    await waitFor(() => {
        expect(screen.getByText(/Balance:/i)).toBeInTheDocument();
    }, { timeout: 5000, interval: 50 });
})