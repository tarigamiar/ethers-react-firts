import Transfer from '../components/transfer';
import { render, screen, fireEvent, waitFor, queryByText } from '@testing-library/react';
import userEvent from "@testing-library/user-event"

jest.setTimeout(30000);

test('test transfert', async () => {
    render(<Transfer />);
    const amount = '0.0001';
    const submit = screen.getByText(/Submit/i);
    userEvent.click(submit);
    let linkElement = screen.getByText(/Please enter a valid address/i);
    expect(linkElement).toBeInTheDocument();
    userEvent.type(screen.getByPlaceholderText(/Your account public address/i), '0xDcFd04a00c582AB5F6B908742bDC2850CBD3CB77');
    userEvent.type(screen.getByPlaceholderText(/Other account address/i), '0x160694f252B907CDf3862922950142a5f1Fd161a');
    userEvent.type(screen.getByPlaceholderText(/Your private key/i), '********');
    userEvent.type(screen.getByPlaceholderText(/Amount/i), amount);
    userEvent.click(submit);
    linkElement = screen.getByText(/wait for result../i);
    expect(linkElement).toBeInTheDocument();
    await waitFor(() => {
        expect(screen.getByText(/wait for blocks confirmation../i)).toBeInTheDocument();
    }, { timeout: 15000, interval: 50 });
    await waitFor(() => {
        expect(screen.getByText(/Sender balance before:/i)).toBeInTheDocument();
    }, { timeout: 15000, interval: 50 });
});