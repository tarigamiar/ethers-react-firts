import Fields from '../components/fields';
import { render, screen } from '@testing-library/react';

test('render fields', () => {
    render(<Fields ready={true} testTest="testText"/>);
    let linkElement = screen.getByText(/testText/i);
    expect(linkElement).toBeInTheDocument();
    linkElement = screen.getByText(/Test test:/i);
    expect(linkElement).toBeInTheDocument();
    render(<Fields ready={false} notReadyText="not_ready"/>);
    linkElement = screen.getByText(/not_ready/i);
    expect(linkElement).toBeInTheDocument();
});