import { Band } from './components/band.jsx'
import React from 'react';
import Contract from './components/contract.jsx'
import Balance from './components/balance.jsx'
import './app.css'
import Transfer from './components/transfer.jsx'
import { ThemeContext } from './context/themeContext.jsx';
import { themes } from './context/themeContext.jsx';
import Toolbar from './components/toolbar.jsx'

function App() {
  const [theme, setTheme] = React.useState(themes.light);

  const toggleTheme = () => {
    setTheme(theme === themes.dark ? themes.light : themes.dark);
  }

  return (
    <div className="container" style={{backgroundColor: theme.background, color: theme.color}}>
      <Band text="Ether.js application Kovan test net"/>
      <ThemeContext.Provider value={theme}>
        <Toolbar toggleTheme={toggleTheme} />
        <Balance />
        <Contract />
        <Transfer />
      </ThemeContext.Provider>
    </div>
  );
}

export default App;
