import React from "react";

export const themes = {
  light: {
    color: 'black',
    background: '#ecf0f1',
  },
  dark: {
    color: 'white',
    background: '#2a527a',
  },

};

export const ThemeContext = React.createContext(themes.light);